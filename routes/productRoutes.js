const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Create Product (Admin Only)
router.post("/create", (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.addProduct(data).then((result) => {
		response.send(result)
	})
})

// Retrieve ALL products (Admin Only) (Added feature)
router.get("/", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	ProductController.getAllProducts(data).then((result) => {
		response.send(result)
	})
})


// Retrieve ALL ACTIVE products
router.get("/all-active-products", (request, response) => {
	ProductController.getAllActiveProducts().then((result) => {
		response.send(result)
	})
})

// Get SINGLE product
router.get("/:productId", (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

// UPDATE product information(Admin Only)
router.put("/:productId/update", auth.verify, (request, response) => {
	let data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		productId: request.params.productId,
		newData: request.body
	}
	ProductController.updateProduct(data).then((result) => {
		response.send(result)
	})
})

// ARCHIVE product (Admin Only)
router.delete("/:productId/archive", auth.verify, (request, response) => {
	let data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		productId: request.params.productId
	}
	ProductController.archiveProduct(data).then((result) => {
		response.send(result)
	})
})






module.exports = router