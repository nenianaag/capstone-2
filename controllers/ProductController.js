const Product = require('../models/Product')

// Create Product
module.exports.addProduct = (data) => {

	return Product.findOne({name: data.product.name}).then((result) => {

		if(result != null){
			return {message: 'Duplicate product found!'}
		} else {

			if(data.isAdmin){
				let newProduct = new Product({
					name: data.product.name,
					description: data.product.description,
					price: data.product.price
				})

				return newProduct.save().then((newProduct, error) => {
					if(error){
						return false
					}

					return {message: 'New product has been added successfully!'}
				})
			}

			let message = Promise.resolve({
				message: 'User must be an Admin to access this.'
			})

			return message.then((value) => {
				return value
			})
		}
	})	
}

// Retrieve ALL products (Added feature)
module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
}


// Retrieve ALL ACTIVE products
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then((result) =>{
		return result
	})
}

// Get SINGLE product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result) => {
		return result
	})
}

// UPDATE product information(Admin Only)
module.exports.updateProduct = (data) => {
	if(data.isAdmin){
		return Product.findByIdAndUpdate(data.productId, {
			name: data.newData.name,
			description: data.newData.description,
			price: data.newData.price
		}).then((updatedProduct, error) => {
			if(error){
				return false
			} else {
				return {message: 'Product has been updated successfully!'}
			}
		})
	} else {
		let message = Promise.resolve({
			message: 'You must be an Admin to update products.'
		})

		return message.then((value) => {
			return value
		})
	}	
}

// ARCHIVE product (Admin Only)
module.exports.archiveProduct = (data) => {
	if(data.isAdmin){
		return Product.findById(data.productId).then((result) =>{
			return Product.findByIdAndUpdate(data.productId, {isActive: !result.isActive}).then((archiveProduct, error) => {
				if(error) {
					return false
				} else {
					let status = "unarchived";

					if (result.isActive) {
						status = "archived";
					}

					return {message: `The ${result.name} has been ${status} successfully`}
				}
			})
		})
		
	} else {
		let message = Promise.resolve({
			message: 'You must be an Admin to update products.'
		})

		return message.then((value) => {
			return value
		})
	}	
}