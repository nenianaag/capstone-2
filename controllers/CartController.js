const Cart = require('../models/Cart')
const Order = require('../models/Order')

// Add product
module.exports.addCartProduct = (data) => {

	if(data.isAdmin){
		let message = Promise.resolve({
			message: 'You are not allowed to access this.'
		})

		return message.then((value) => {
			return value
		})

	} else {

		return Cart.find({userId: data.userId, productId: data.product.productId}).then((result) => {

			if(result.length === 0){

				let newCart = new Cart({
					userId: data.userId,
					productId: data.product.productId,
					quantity: data.product.quantity,
					subtotal: data.product.subtotal
				})

				return newCart.save().then((newCart, error) => {

					if(error){
						return false
					}
					
					return {message: 'Cart product successfully added!'}

				})

			} else {

				let message = Promise.resolve({
					message: 'Cart Product already exists.'
				})

				return message.then((value) => {
					return value
				})
			}
		})
	}
}


// Change product quantities from cart & subtotal for each item
module.exports.updateCartProduct = (data) => {
	return Cart.find({_id: data.cartProductId}).then((cartProduct) => {

		if(cartProduct.length === 0){

			let message = Promise.resolve({
				message: 'Cart Product not found.'
			})

			return message.then((value) => {
				return value
			})

		} else {

			return Cart.findByIdAndUpdate(data.cartProductId, {
				quantity: data.quantity,
				subtotal: data.subtotal
			}).then((result, error) => {

				if (error) {
					return { message: "Cart product update unsuccesful."}
				}

				return { message: "Cart product successfully updated"}
			})
		}
	})
}

// Remove products from cart
module.exports.deleteCartProduct = (data) => {

	return Cart.findByIdAndDelete(data).then((result) => {
		if(result == null) {
			let message = Promise.resolve({
				message: 'Cart is empty.'
			})

			return message.then((value) => {
				return value
			})
		} else {
			return { message: "Cart product successfully deleted"};	
		}
	})
}

//Total price for all items (checkout)
module.exports.checkout = (data) => {
	return Cart.find({ userId: data.userId }).then((result) => {

		if (result.length == 0) {
			let message = Promise.resolve({
				message: 'Cart is empty.'
			})

			return message.then((value) => {
				return value
			})
		} else {

			let totalPrice = 0;
			let products = []

			result.forEach((cartProduct) => {
				totalPrice += cartProduct.subtotal;
				products.push({
					productId: cartProduct._id,
					quantity: cartProduct.quantity
				})

				Cart.findByIdAndDelete(cartProduct._id).then((result, error) => {
					if(error){
						return false
					}
				});
			})

			let newOrder = new Order({
				userId: data.userId,
				products: products,
				totalAmount: totalPrice
			})

			return newOrder.save().then((newOrder, error) => {
				if(error){
					return false
				}

				return {message: `Order successfully created total amount is ${totalPrice}`}
			})
		}
	})
}